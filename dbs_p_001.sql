CREATE SCHEMA dbs_p;

CREATE TABLE dbs_p.country (
	_country_name varchar(60),
    _country_code varchar(3),
    PRIMARY KEY(_country_name)
);

CREATE TABLE dbs_p.co2_emission (
	_year INTEGER,
    _amount INTEGER,
    _country_name varchar(60),
    FOREIGN KEY(_country_name) REFERENCES country(_country_name) ON DELETE CASCADE,
    PRIMARY KEY(_year, _country_name)
);

CREATE TABLE dbs_p.population (
	_year INTEGER,
    _annual_growth INTEGER,
    _total_count INTEGER,
    _country_name varchar(60),
    FOREIGN KEY(_country_name) REFERENCES country(_country_name) ON DELETE CASCADE,
    PRIMARY KEY(_year, _country_name)
);

CREATE TABLE dbs_p.gdp (
	_year INTEGER,
    _value INTEGER,
    _country_name varchar(60),
    FOREIGN KEY(_country_name) REFERENCES country(_country_name) ON DELETE CASCADE,
    PRIMARY KEY(_year, _country_name)
);

CREATE TABLE dbs_p.life_info (
	_year INTEGER,
    _status varchar(15),
    _life_expectancy INTEGER,
    _schooling INTEGER,
    _bmi INTEGER,
    _country_name varchar(60),
    FOREIGN KEY(_country_name) REFERENCES country(_country_name) ON DELETE CASCADE,
    PRIMARY KEY(_year, _country_name)
);
